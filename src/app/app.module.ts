import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';
import { WelcomePage } from '../pages/welcome/welcome';
import { TinTucPage } from '../pages/tin-tuc/tin-tuc';
import { DeckOfCards } from '../components/card/deck-of-cards';
import { TinderCard } from '../components/card/tinder-card';
import { HomePage as TinderCardHomePage } from '../pages/page-tinder-card/home';
import { HorizontalEdgePanGestureController } from '../utils/gestures/horizontal-edge-pan-gesture';
import { PanGestureController } from '../utils/gestures/pan-gesture';
import { PinchGestureController } from '../utils/gestures/pinch-gesture';
import { PressGestureController } from '../utils/gestures/press-gesture';
import { RotateGestureController } from '../utils/gestures/rotate-gesture';
import { SwipeGestureController } from '../utils/gestures/swipe-gesture';
import { TapGestureController } from '../utils/gestures/tap-gesture';
import { PoochProvider } from '../pages/page-tinder-card/dog-data-provider';
import { HammerFactory } from '../utils/gestures/hammer-factory';

@NgModule({
  declarations: [
    MyApp,
    Page1,
    Page2,
    WelcomePage,
    TinTucPage,
    DeckOfCards,
    TinderCard,
    TinderCardHomePage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Page1,
    Page2,
    WelcomePage,
    TinTucPage,
    TinderCardHomePage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    HorizontalEdgePanGestureController,
    PanGestureController,
    PinchGestureController,
    PressGestureController,
    RotateGestureController,
    SwipeGestureController,
    TapGestureController,
    PoochProvider,
    HammerFactory
  ]
})
export class AppModule { }
