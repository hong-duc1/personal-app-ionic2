import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TinTucPage } from '../tin-tuc/tin-tuc';

@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})
export class WelcomePage {

  private pages: Array<{ image: string, component: any, title: string }>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.pages = [
      { title: 'Tin mới mỗi ngày', image: 'assets/image/card-sf.png', component: TinTucPage }
    ]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WelcomePage');
  }

  goToPage(page: any) {
    this.navCtrl.setRoot(page.component);
  }

}
